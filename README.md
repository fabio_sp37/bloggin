# Bloggin!

Aplicativo mobile híbrido desenvolvido para **Blogueiros**. As funcionalidades vão desde o cadastro de parceiros e campanhas até gerenciamento de cupons de desconto (em breve).


# Download de dependências
Após clonar o projeto, na pasta **bloggin** execute:

	> npm install

## Executar

Utilize a linha de comando para executar:

	> tns run android

## Tecnologias
- Svelte-navite
- Nativescript
- Firebase
